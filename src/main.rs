use anyhow::{bail, Context, Result};
use bttv::HighlightKeyword;
use net::api::UserName;

use crate::net::api::TwitchAPI;

mod bttv;
mod net;
mod utils;

fn main() -> Result<()> {
    let config = utils::get_args();

    let bttv_file = config.bttv_file.as_ref().unwrap();
    let mut bttv_settings =
        bttv::Settings::open(bttv_file).with_context(|| format!("Unable to read BTTV settings file: {bttv_file:?}"))?;

    let twitch = net::get_token().context("Unable to retrieve Twitch token: {msg}")?;

    let followed_users = twitch.get_user_follows(None).context("Unable to get followed users")?;

    eprintln!(
        "Comparing {} BetterTTV highlight keywords with {} followed channels (similarity: {})",
        bttv_settings.highlight_keywords.len(),
        followed_users.len(),
        config.threshold
    );

    let mut new_list = HighlightList::new(followed_users);
    for keyword in bttv_settings.highlight_keywords.iter() {
        new_list
            .add_bttv_keyword(keyword, config.threshold, &twitch)
            .with_context(|| format!("Unable to add BTTV keyword: {keyword}"))?;
    }

    let output_file = config.output_file.as_ref().unwrap();
    bttv_settings
        .save_new_highlights(output_file, new_list.to_bttv_keyword_list())
        .with_context(|| format!("Unable to save output file: {output_file:?}"))
}

struct HighlightList {
    users: Vec<(String, Option<String>)>,
    messages: Vec<String>,
}

impl HighlightList {
    pub fn new(followed_users: Vec<UserName>) -> Self {
        Self {
            users: followed_users.into_iter().map(|u| (u.name, Some(u.id))).collect(),
            messages: Vec::new(),
        }
    }

    pub fn add_bttv_keyword(&mut self, keyword: &HighlightKeyword, sim_thresh: f64, twitch: &TwitchAPI) -> Result<()> {
        let user = match keyword {
            HighlightKeyword::Message(m) => {
                eprintln!("Adding message to highlight list: {m}");
                self.messages.push(m.clone());
                return Ok(());
            }
            HighlightKeyword::Username(u) => u,
        };
        let user_lower = user.to_lowercase();
        match self.search_user(&user_lower, sim_thresh) {
            Ok((_, sim)) => eprintln!("Searched and found user '{user}' in list (similarity = {sim}), skipping"),
            Err((u, sim)) => {
                eprintln!("Unable to find match for BTTV user '{user}' (closest followed user is '{u}' with similarity = {sim}), searching...");
                let mut channels = twitch
                    .search_channels(&user, 1)
                    .with_context(|| format!("Unable to search channels for: {user}"))?;
                if channels.is_empty() {
                    bail!("No channels found for user: {user}");
                }
                let closest_match = channels.swap_remove(0);
                eprintln!("Found {} users (assuming first: {closest_match}", channels.len() + 1);

                let user_input = utils::get_input("Add to highlight list? ([Y]es, [N]o, Yes & [O]pen url) ")?
                    .chars()
                    .flat_map(char::to_lowercase)
                    .next();

                match user_input {
                    Some('y') => (),
                    Some('o') => utils::open_url(closest_match.get_url()),
                    Some('n') => return Ok(()),
                    _ => {
                        eprintln!("Unknown input given, assuming No");
                        return Ok(());
                    }
                }
                self.users.push((closest_match.name, Some(closest_match.id)));
            }
        }
        Ok(())
    }

    pub fn to_bttv_keyword_list(mut self) -> Vec<HighlightKeyword> {
        self.messages.sort();
        self.messages.dedup();
        eprintln!("Message highlight list after deduplication: {}", self.messages.len());

        self.users.sort_by_key(|(u, _)| u.to_lowercase());
        self.users.dedup_by(|u1, u2| u1.0 == u2.0);
        eprintln!("User highlight list after deduplication: {}", self.users.len());

        self.messages
            .into_iter()
            .map(HighlightKeyword::Message)
            .chain(self.users.into_iter().map(|(u, _)| HighlightKeyword::Username(u)))
            .collect()
    }

    fn search_user(&self, user: &str, sim_thresh: f64) -> Result<(&str, f64), (&str, f64)> {
        let (max_user, max_sim) = self.users.iter().fold(("", 0.0), |(max_user, max_sim), (u, _)| {
            let jw_sim = utils::calculate_jw_similarity(&u.to_lowercase(), &user);
            if jw_sim >= max_sim {
                (&u, jw_sim)
            } else {
                (max_user, max_sim)
            }
        });
        if max_sim >= sim_thresh {
            Ok((max_user, max_sim))
        } else {
            Err((max_user, max_sim))
        }
    }
}
