use std::{fmt, fs, path::Path};

use anyhow::{bail, Context, Result};
use serde_json::{json, Value as JsonValue};

pub enum HighlightKeyword {
    Username(String),
    Message(String),
}

impl HighlightKeyword {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Username(s) => s,
            Self::Message(s) => s,
        }
    }

    pub fn to_json(self, id: usize) -> JsonValue {
        let (type_, keyword) = match self {
            HighlightKeyword::Username(user) => (3, user),
            HighlightKeyword::Message(msg) => (0, msg),
        };
        json!({
            "id": id,
            "status": None::<String>,
            "type": type_,
            "keyword": keyword
        })
    }
}
impl fmt::Display for HighlightKeyword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            HighlightKeyword::Username(s) => write!(f, "({s})"),
            HighlightKeyword::Message(s) => write!(f, "{{{s}}}"),
        }
    }
}

pub struct Settings {
    root: JsonValue,
    pub highlight_keywords: Vec<HighlightKeyword>,
}

impl Settings {
    pub fn open(file: &Path) -> Result<Self> {
        eprintln!("Reading BetterTTV settings file: {file:?}");
        let root =
            serde_json::from_slice::<JsonValue>(&fs::read(file).context("Unable to open BetterTTV settings file")?)
                .context("Unable to parse JSON")?;

        let mut highlight_keywords = root
            .get("bttv_settings")
            .context("JSON missing path: /bttv_settings")?
            .get("highlightKeywords")
            .context("JSON missing path:  /bttv_settings/highlightKeywords")?
            .as_object()
            .context("JSON value '/bttv_settings/highlightKeywords' is not an object")?
            .iter()
            .map(|(key, obj)| {
                parse_highlight_json_entry(obj)
                    .with_context(|| format!("Unable to parse highlight keyword entry: {key}"))
            })
            .collect::<Result<Vec<HighlightKeyword>>>()?;

        highlight_keywords.sort_by_key(|hk| hk.as_str().to_string());

        eprintln!("Parsed {} previous highlight keywords", highlight_keywords.len());
        return Ok(Self {
            root,
            highlight_keywords,
        });
    }

    pub fn save_new_highlights(&mut self, file: &Path, highlight_list: Vec<HighlightKeyword>) -> Result<()> {
        let highlight_obj = self
            .root
            .get_mut("bttv_settings")
            .unwrap()
            .get_mut("highlightKeywords")
            .unwrap()
            .as_object_mut()
            .unwrap();
        highlight_obj.clear();

        for (i, keyword) in highlight_list.into_iter().enumerate() {
            highlight_obj.insert((i + 1).to_string(), keyword.to_json(i + 1));
        }

        fs::write(
            &file,
            serde_json::to_string_pretty(&self.root).context("Unable to format JSON")?,
        )
        .context("Unable to write to file file")?;

        eprintln!("New highlight list written to: {file:?}");
        return Ok(());
    }
}

fn parse_highlight_json_entry(obj: &JsonValue) -> Result<HighlightKeyword> {
    let obj = obj.as_object().context("Not an object")?;
    let highlight_keyword = obj
        .get("keyword")
        .context("Does not have key: keyword")?
        .as_str()
        .context("Value for key 'keyword' is not a string")?
        .to_string();
    let highlight_type = match obj
        .get("type")
        .context("Does not have key: type")?
        .as_i64()
        .context("Value for key 'type' is not an integer")?
    {
        0 => HighlightKeyword::Message(highlight_keyword),
        3 => HighlightKeyword::Username(highlight_keyword),
        x => bail!("Unknown highlight keyword type: {x}"),
    };
    return Ok(highlight_type);
}
