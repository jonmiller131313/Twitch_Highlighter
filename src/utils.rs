use std::{
    cmp::{max, min},
    io,
    iter::zip,
    path::PathBuf,
};

use anyhow::{Context, Result};
use clap::{error::ErrorKind, CommandFactory, Parser};
use url::Url;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
/// Logs into your Twitch account, retrieves all of your followed accounts, and appends to a
/// BetterTTV backup file to highlight messages from those users (if not already highlighting).
pub struct CLIParams {
    /// Edit in-place
    #[arg(short, long, value_name = "EXT")]
    pub in_place: bool,

    /// Username similarity threshold, must be between 0 and 1
    #[arg(short, long, default_value = "0.85")]
    pub threshold: f64,

    /// BetterTV settings .backup file. If not specified or is '-' then reads from stdin
    pub bttv_file: Option<PathBuf>,

    /// Output file to write. Ignored if -i is used. If not specified or is '-' then prints to stdout
    pub output_file: Option<PathBuf>,
}

fn arg_error(msg: &str) -> ! {
    CLIParams::command().error(ErrorKind::ArgumentConflict, msg).exit();
}

pub fn get_args() -> CLIParams {
    let mut config = CLIParams::parse();
    if config.threshold < 0.0 || config.threshold > 1.0 {
        arg_error("Similarity threshold must be between 0 and 1");
    }
    config.bttv_file = config
        .bttv_file
        .filter(|p| p != &PathBuf::from("-"))
        .or_else(|| Some(PathBuf::from("/dev/stdin")));

    if config.in_place {
        config.output_file = config.bttv_file.clone();
    } else {
        config.output_file = config
            .output_file
            .filter(|p| p != &PathBuf::from("-"))
            .or_else(|| Some(PathBuf::from("/dev/stdout")));
    }

    return config;
}

pub fn open_url(url: Url) {
    eprintln!("Opening URL for authorization: {}", &url);
    if let Err(e) = open::that(url.as_str()) {
        eprintln!("Unable to open, please open manually: {e}");
    }
}

// Jaro-Winkler
pub fn calculate_jw_similarity(s1: &str, s2: &str) -> f64 {
    if s1 == s2 {
        return 1.0;
    } else if !s1.is_ascii() || !s2.is_ascii() {
        return 0.0;
    }

    let max_char_distance = max(s1.len(), s2.len()) / 2 - 1;

    let matching_chars = {
        let mut char_bitmaps = [vec![None; s1.len()], vec![None; s2.len()]];
        for (i1, c1) in s1.char_indices() {
            let start = min(s2.len(), max(0, i1 as isize - max_char_distance as isize) as usize);
            let end = min(s2.len(), i1 + max_char_distance + 1);
            for (i2, c2) in s2[start..end].char_indices() {
                if c1 == c2 {
                    char_bitmaps[0][i1] = Some(c1);
                    char_bitmaps[1][start + i2] = Some(c2);
                    break;
                }
            }
        }
        char_bitmaps[0]
            .iter()
            .filter_map(|c| *c)
            .zip(char_bitmaps[1].iter().filter_map(|c| *c))
            .collect::<Vec<(char, char)>>()
    };

    if matching_chars.is_empty() {
        return 0.0;
    }

    let t = matching_chars.iter().filter(|(c1, c2)| c1 != c2).count() as f64 / 2.0;

    let m = matching_chars.len() as f64;
    let l = zip(s1[..min(s1.len(), 4)].chars(), s2[..min(s2.len(), 4)].chars())
        .fuse()
        .filter(|(c1, c2)| c1 == c2)
        .count() as f64;
    let p = 0.1;
    let s1 = s1.len() as f64;
    let s2 = s2.len() as f64;
    let jaro_sim = (1.0 / 3.0) * (m / s1 + m / s2 + (m - t) / m);
    return jaro_sim + l * p * (1.0 - jaro_sim);
}

pub fn get_input(prompt: &str) -> Result<String> {
    let mut input = String::new();

    eprint!("{prompt}");
    let _ = io::stdin().read_line(&mut input).context("Unable to read user input")?;
    return Ok(input.trim().into());
}
