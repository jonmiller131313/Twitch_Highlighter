use std::fs;
use std::io;
use std::io::ErrorKind::UnexpectedEof;

use rpassword::prompt_password;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Secret {
    pub id: String,
    pub secret: String,
}

impl Secret {
    pub fn get(secrets_file: Option<String>, err_from_file: bool) -> Option<Secret> {
        if let Some(secrets_file) = secrets_file {
            log::info!("Reading secrets file: {secrets_file}");
            match fs::read(if secrets_file == "-" {
                "/dev/stdin"
            } else {
                &secrets_file
            }) {
                Ok(bytes) => match toml::from_slice::<Secret>(&*bytes) {
                    Ok(data) => return Some(data),
                    Err(e) => log::error!("Unable to parse TOML secrets file '{secrets_file}': {e}")
                }
                Err(e) => log::error!("Unable to open TOML secrets file '{secrets_file}': {e}")
            }
            if err_from_file {
                log::error!("Assuming running in a script. Quitting");
                return None;
            } else {
                log::info!("Falling back to user prompt");
            }
        }
        eprintln!("You can find/register your API key here: https://dev.twitch.tv/console/apps/");
        eprint!("Application ID: ");
        let id = {
            let mut id = String::new();
            match io::stdin().read_line(&mut id) {
                Ok(0) => return None,
                Ok(_) => id.trim().to_string(),
                Err(e) => {
                    log::error!("Unable to read application ID from stdin: {e}");
                    return None;
                }
            }
        };
        let secret = match prompt_password("Application Secret: ") {
            Ok(secret) => secret,
            Err(e) => {
                if e.kind() != UnexpectedEof {
                    log::error!("\nUnable to read application password from stdin: {e}");
                }
                return None;
            }
        };
        return Some(Secret {
            id,
            secret,
        });
    }
}
