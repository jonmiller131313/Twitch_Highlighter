use std::{
    collections::HashMap,
    sync::{mpsc, Mutex},
    thread,
};

use actix_web::{
    dev::ServerHandle,
    rt::Runtime,
    web::{self, Data},
    App, HttpRequest, HttpResponse, HttpServer,
};

pub struct AuthCodeNegotiator {
    pub port: u16,
    srv_handle: ServerHandle,
    srv_thd: thread::JoinHandle<()>,
    token_receiver: mpsc::Receiver<String>,
}

impl AuthCodeNegotiator {
    pub fn start(state: String) -> Self {
        let port = 3000;
        let (tx, rx) = mpsc::channel();
        let srv_data = Data::new(Mutex::new(ServerData {
            state,
            token_sender: tx,
        }));

        let srv = HttpServer::new(move || {
            App::new()
                .app_data(Data::clone(&srv_data))
                .route("/", web::get().to(redirect_callback))
        })
        .workers(1)
        .bind(("127.0.0.1", port))
        .expect("Unable to create web service?")
        .run();

        let srv_handle = srv.handle();

        let srv_thd = thread::Builder::new()
            .name("Server".to_string())
            .spawn(move || {
                Runtime::new()
                    .expect("Unable to start async server runtime?")
                    .block_on(async {
                        srv.await.expect("Unable to start server?");
                    });
            })
            .expect("Unable to spawn server thread?");

        AuthCodeNegotiator {
            port,
            srv_handle,
            srv_thd,
            token_receiver: rx,
        }
    }

    pub fn wait_for_token(self) -> String {
        let token = self.token_receiver.recv().expect("Unable to receive token?");
        Runtime::new()
            .expect("Unable to start async runtime to stop server?")
            .block_on(async {
                self.srv_handle.stop(false).await;
            });
        self.srv_thd.join().expect("Web service crashed?");
        token
    }
}

struct ServerData {
    state: String,
    token_sender: mpsc::Sender<String>,
}

fn split_query_params(query: &str) -> HashMap<&str, &str> {
    query
        .split("&")
        .map(|q| q.split_once("=").unwrap_or_default())
        .filter(|&k_v| k_v != Default::default())
        .collect()
}

async fn redirect_callback(req: HttpRequest, data: Data<Mutex<ServerData>>) -> HttpResponse {
    let data = data.lock().expect("Unable to get server data?");
    let args = split_query_params(req.query_string());

    if *args.get("state").expect("Twitch redirect did not have state?") != data.state {
        panic!("Twitch redirect state not equal?");
    } else if let Some(&token) = args.get("code") {
        data.token_sender
            .send(token.to_string())
            .expect("Unable to send token?");
    } else {
        panic!(
            "Twitch returned error[{}]: {}",
            args.get("error").expect("Twitch redirect didn't not have error?"),
            args.get("error_description")
                .expect("Twitch redirect did not have error description?")
        )
    }
    HttpResponse::Ok().body("You can close this tab")
}
