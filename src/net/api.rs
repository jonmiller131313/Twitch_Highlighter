use std::{fmt, str::FromStr};

use anyhow::{Context, Result};
use reqwest::{
    blocking::Client,
    header::{HeaderName, HeaderValue},
};
use url::Url;

use super::{response_types::*, CLIENT_ID};

pub struct TwitchAPI {
    req_client: Client,
    user_id: String,
}

pub struct UserName {
    pub id: String,
    pub name: String,
    login: String,
}
impl From<UserFollowsDetails> for UserName {
    fn from(details: UserFollowsDetails) -> Self {
        Self {
            id: details.broadcaster_id,
            name: details.broadcaster_name,
            login: details.broadcaster_login,
        }
    }
}
impl From<SearchChannelsDetails> for UserName {
    fn from(details: SearchChannelsDetails) -> Self {
        Self {
            id: details.id,
            name: details.display_name,
            login: details.broadcaster_login,
        }
    }
}

impl UserName {
    pub fn get_url(&self) -> Url {
        Url::parse(&format!("https://www.twitch.tv/{}", self.login)).unwrap()
    }
}

impl fmt::Display for UserName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}[{}/{}]", self.name, self.login, self.id)
    }
}

const GET_USER_INFO: &str = "https://api.twitch.tv/helix/users";
const GET_FOLLOWED: &str = "https://api.twitch.tv/helix/channels/followed";
const SEARCH_CHANNELS: &str = "https://api.twitch.tv/helix/search/channels";

impl TwitchAPI {
    pub fn new(token: String) -> Result<Self> {
        let req_client = Client::builder()
            .default_headers(
                [
                    ("Authorization", format!("Bearer {token}").as_str()),
                    ("Client-Id", CLIENT_ID),
                ]
                .into_iter()
                .map(|(header, value)| {
                    (
                        HeaderName::from_str(header).expect("Invalid HTTP header name?"),
                        HeaderValue::from_bytes(value.as_bytes()).expect("Invalid HTTP header values?"),
                    )
                })
                .collect(),
            )
            .build()
            .context("Unable to create reqwest client")?;

        let mut api = Self {
            req_client,
            user_id: String::new(),
        };

        let user_info = api.get_user_infos(None).context("Unable to get user info")?;
        api.user_id.push_str(&user_info.data[0].id);
        eprintln!("Got account information for user: {}", &user_info.data[0].display_name);

        eprintln!("User ID: {}", api.user_id);
        return Ok(api);
    }

    pub fn get_user_infos(&self, ids: Option<Vec<&str>>) -> Result<UserInfo> {
        Ok(self
            .req_client
            .get(GET_USER_INFO)
            .query(
                &ids.iter()
                    .flatten()
                    .map(|&id| ("user_id", id))
                    .collect::<Vec<(&str, &str)>>(),
            )
            .send()
            .context("Unable to send request")?
            .error_for_status()
            .context("Response not OK from twitch")?
            .json()
            .context("Unable to parse response JSON")?)
    }

    pub fn get_user_follows(&self, id: Option<&str>) -> Result<Vec<UserName>> {
        let id = id.unwrap_or(&self.user_id);
        let mut users = Vec::new();

        let request = self
            .req_client
            .get(GET_FOLLOWED)
            .query(&[("user_id", id), ("first", "100")]);
        let mut followers_count = 0;
        let mut followers_total = 1;
        let mut page = None;

        while followers_count < followers_total {
            let user_follows = request
                .try_clone()
                .unwrap()
                .query(&page.iter().map(|p: &String| ("after", p.as_str())).collect::<Vec<_>>())
                .send()
                .context("Unable to get request")?
                .error_for_status()
                .context("Response not OK from Twitch")?
                .json::<UserFollows>()
                .context("Unable to parse JSON")?;

            followers_total = user_follows.total;
            followers_count += user_follows.data.len();
            page = user_follows.pagination.cursor;

            users.extend(user_follows.data.into_iter().map(UserName::from));
        }
        users.sort_by_key(|u| u.name.to_lowercase());
        eprintln!("Retrieved {} followed channels", users.len());
        return Ok(users);
    }

    pub fn search_channels(&self, user: &str, num_results: usize) -> Result<Vec<UserName>> {
        Ok(self
            .req_client
            .get(SEARCH_CHANNELS)
            .query(&[("query", user)])
            .query(&[("first", num_results)])
            .send()
            .context("Unable to send request")?
            .error_for_status()
            .context("Response not OK from twitch")?
            .json::<SearchChannels>()
            .context("Unable to parse JSON")?
            .data
            .into_iter()
            .map(UserName::from)
            .collect())
    }
}
