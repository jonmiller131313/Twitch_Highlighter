use serde::Deserialize;

#[derive(Deserialize)]
pub struct UserInfo {
    pub data: Vec<UserInfoDetails>,
}

#[derive(Deserialize)]
pub struct UserInfoDetails {
    pub id: String,
    pub login: String,
    pub display_name: String,
    #[serde(rename(deserialize = "type"))]
    pub type_: String,
    pub broadcaster_type: String,
    pub description: String,
    pub profile_image_url: String,
    pub offline_image_url: String,
    pub view_count: u128,
    pub email: Option<String>,
    pub created_at: String,
}

#[derive(Deserialize)]
pub struct UserFollows {
    pub total: usize,
    pub data: Vec<UserFollowsDetails>,
    pub pagination: UserFollowsPagination,
}

#[derive(Deserialize)]
pub struct UserFollowsDetails {
    pub broadcaster_id: String,
    pub broadcaster_login: String,
    pub broadcaster_name: String,
    pub followed_at: String,
}

#[derive(Deserialize)]
pub struct UserFollowsPagination {
    pub cursor: Option<String>,
}

#[derive(Deserialize)]
pub struct SearchChannels {
    pub data: Vec<SearchChannelsDetails>,
}

#[derive(Deserialize)]
pub struct SearchChannelsDetails {
    pub broadcaster_language: String,
    pub broadcaster_login: String,
    pub display_name: String,
    pub game_id: String,
    pub game_name: String,
    pub id: String,
    pub is_live: bool,
    pub tag_ids: Vec<String>,
    pub thumbnail_url: String,
    pub title: String,
    pub started_at: String,
}
