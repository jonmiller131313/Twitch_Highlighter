use anyhow::{Context, Result};
use oauth2::CsrfToken as StateToken;
use reqwest::blocking::Client;
use serde::Deserialize;
use url::Url;

use redirect_callback::AuthCodeNegotiator;

use crate::utils::open_url;

pub mod api;
mod redirect_callback;
mod response_types;

static CLIENT_ID: &str = "c080t2ep9jnzqra5otqxyqoac5djle";
static CLIENT_SECRET: &str = "cchz9spn47vszpoy37i2jfmupgsgkc";
static AUTH_URL: &str = "https://id.twitch.tv/oauth2/authorize";
static TOKEN_URL: &str = "https://id.twitch.tv/oauth2/token";

pub fn get_token() -> Result<api::TwitchAPI> {
    let state_value = StateToken::new_random().secret().clone();
    eprintln!("State value: {}", state_value);
    let negotiator = AuthCodeNegotiator::start(state_value.clone());
    let redirect_uri = format!("http://localhost:{}", negotiator.port);

    let auth_url = Url::parse_with_params(
        AUTH_URL,
        &[
            ("response_type", "code"),
            ("client_id", CLIENT_ID),
            ("redirect_uri", redirect_uri.as_str()),
            ("state", state_value.as_str()),
            ("scope", "user:read:follows"),
        ],
    )
    .context("Unable to format URL")?;

    open_url(auth_url);
    let code = negotiator.wait_for_token();

    let token_response: TokenRequestResponse = Client::new()
        .post(TOKEN_URL)
        .form(
            &[
                ("code", code.as_str()),
                ("client_id", CLIENT_ID),
                ("client_secret", CLIENT_SECRET),
                ("redirect_uri", redirect_uri.as_str()),
                ("grant_type", "authorization_code"),
            ]
            .into_iter()
            .collect::<Vec<_>>(),
        )
        .send()
        .context("Unable to request token from auth code")?
        .json()
        .context("Unable to parse JSON")?;

    return Ok(api::TwitchAPI::new(token_response.access_token).context("Unable to initialize Twitch data")?);
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct TokenRequestResponse {
    pub access_token: String,
    pub expires_in: usize,
    pub refresh_token: String,
    pub scope: Vec<String>,
    pub token_type: String,
}
